package com.danybot.CommunicationInterface;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.danybot.CommonUtils.SystemUtils;
import com.danybot.Parser.Binary;
import com.fazecast.jSerialComm.SerialPort;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Serial {

	public static List<String> comPort = new ArrayList<String>();

	public static List<String> getAvailablePorts() {
		if (comPort.isEmpty()) {
			refreshPort();
		}
		return comPort;
	}

	public static void refreshPort() {
		comPort.clear();
		log.info("Scanning serial port...");
		for (SerialPort serialPort : SerialPort.getCommPorts()) {
			comPort.add(serialPort.getSystemPortName());
			log.info(serialPort.getSystemPortName() + '\t' + serialPort.getPortDescription() + '\t'
					+ serialPort.getDescriptivePortName());
		}
		if (comPort.isEmpty()) {
			log.warn("Unable to find the Serial devices");
		}
	}

	public static void sendByte(SerialPort port, byte data) {
		sendBytes(port, data);
	}

	public static void sendShort(SerialPort port, short value) {
		if (value < 0) {
			value = (short) (Binary.twosCompliment(value) | 0x8000);
		}
		sendBytes(port, (byte) (value >> 8), (byte) value);
	}

	public static void sendString(SerialPort port, String str) {
		sendBytes(port, str.getBytes());
	}

	public static void sendBytes(SerialPort port, byte... bytes) {
		if (Objects.nonNull(bytes)) {
			port.writeBytes(bytes, bytes.length);
		}
	}

	public static byte readByte(SerialPort port) {
		return readBytes(port, 1)[0];
	}

	public static short readShort(SerialPort port) {
		byte[] bytes = readBytes(port, 2);
		short value = (short) ((short) bytes[0] << 8 | bytes[1] & 0x00FF);
		if (value < 0) {
			return (short) (Binary.twosCompliment(value) | 0x8000);
		} else {
			return value;
		}
	}

	public static String readString(SerialPort port, int strLength) {
		return new String(readBytes(port, strLength));
	}

	public static byte[] readBytes(SerialPort port, int length) {
		byte[] buffer = new byte[length];
		if (port.bytesAvailable() < length) {
			SystemUtils.waitForMilliSecond(10);
		}
		port.readBytes(buffer, length);
		return buffer;
	}

}
