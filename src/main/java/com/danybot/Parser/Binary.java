package com.danybot.Parser;

public class Binary {

	public static short twosCompliment(short value) {
		return (short) ((short) ~value + 1);
	}
}
